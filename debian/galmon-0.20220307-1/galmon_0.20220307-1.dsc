Format: 3.0 (quilt)
Source: galmon
Binary: galmon
Architecture: any
Version: 0.20220307-1
Maintainer: Patrick Tudor <debian@ptudor.net>
Homepage: https://github.com/ahupowerdns/galmon/
Standards-Version: 4.3.0
Build-Depends: debhelper (>= 11~), help2man, libzstd-dev, adduser
Package-List:
 galmon deb net optional arch=any
Checksums-Sha1:
 41ce01df1d85df1c9b5764c4472d9aae2fbbd7cf 2359292 galmon_0.20220307.orig.tar.xz
 5a84b54dd29d8235f9b0d3210959278792037670 9988 galmon_0.20220307-1.debian.tar.xz
Checksums-Sha256:
 59a5605344deee263bea0663104d7c028ed1017b008eaf9cf5c27fdc8d4fa01d 2359292 galmon_0.20220307.orig.tar.xz
 fab1db3cfd0bbd7804d9193e5898ccfc96579c2db1f232c4ef3113033046f48f 9988 galmon_0.20220307-1.debian.tar.xz
Files:
 945d2f83bcf531e5e75f283455005513 2359292 galmon_0.20220307.orig.tar.xz
 f6d6cc29095a672f70959aa66372f35f 9988 galmon_0.20220307-1.debian.tar.xz
